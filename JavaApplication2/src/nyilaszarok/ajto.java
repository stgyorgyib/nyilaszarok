/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package nyilaszarok;

import java.awt.Color;

/**
 *
 * @author Balázs
 */
public class ajto extends Nyilaszarok2{
    private String tipus;
    
    public ajto(Integer szelesseg, Integer hosszusag, AnyagEnum anyag, Color szin){
        super(szelesseg, hosszusag, anyag, szin);
    }
    
    public String getTipus(){
        return tipus;
    }
    public void setTipus(String tipus){
        this.tipus = tipus;
    }
}
