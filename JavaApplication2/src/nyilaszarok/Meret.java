/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package nyilaszarok;

/**
 *
 * @author Balázs
 */
public class Meret {
    private Integer szelesseg;
    private Integer hosszusag;

    public Meret(Integer szelesseg, Integer hosszusag) {
        this.szelesseg = szelesseg;
        this.hosszusag = hosszusag;
    }

    public Integer getSzelesseg() {
        return szelesseg;
    }

    public Integer getHosszusag() {
        return hosszusag;
    }

    public void setSzelesseg(Integer szelesseg) {
        this.szelesseg = szelesseg;
    }

    public void setHosszusag(Integer hosszusag) {
        this.hosszusag = hosszusag;
    }
    
}
