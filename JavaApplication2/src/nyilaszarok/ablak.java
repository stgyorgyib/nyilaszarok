/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package nyilaszarok;

import java.awt.Color;

/**
 *
 * @author Balázs
 */
public class ablak extends Nyilaszarok2{ 
     private Boolean nyitva;
     
    public ablak(Integer szelesseg,Integer hosszusag, AnyagEnum anyag, Color szin){
        super(szelesseg, hosszusag, anyag, szin);
        this.nyitva = Boolean.FALSE;
    }

    public boolean isNyitva() {
        return this.nyitva;
    }
//    public void setNyitva(Boolean nyitva){ ez nagyon jó, de van ennél jobb is
//        this.nyitva = nyitva;
//    }
    
    public void kinyit(){
        this.nyitva=Boolean.TRUE;
    }
    
    public void becsuk(){
        this.nyitva=Boolean.FALSE;
    }
    
}

