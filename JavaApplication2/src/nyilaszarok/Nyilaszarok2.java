/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package nyilaszarok;

import java.awt.Color;

/**
 *
 * @author Balázs
 */


    public abstract class Nyilaszarok2{
        protected Meret meret;
        protected AnyagEnum anyag;
        protected Color szin;

        public Nyilaszarok2(Integer szelesseg, Integer hosszusag, AnyagEnum anyag, Color szin) {
            this.meret = new Meret(szelesseg, hosszusag);
            this.anyag = anyag;
            this.szin = szin;
        }
        
        
        public Meret getMeret(){
            return meret;
        }
        public Integer getHosszusag(){
            return this.meret.getHosszusag();
        }
        public Integer getSzelesseg(){
            return this.meret.getSzelesseg();
        }
        public AnyagEnum getAnyag(){
            return anyag;
        }
        public Color getSzin(){
            return szin;
        }
        public void csokkentSzelesseg(Integer levagando){
            if(this.meret.getSzelesseg() > 0){
                this.meret.setSzelesseg(this.meret.getSzelesseg() - levagando);
            }
        }
        public void csokkentHosszusag(Integer levagando){
            if(this.meret.getHosszusag() > 0){
                this.meret.setHosszusag(this.meret.getHosszusag() - levagando);
            }
        }
//        public void setAnyag(String anyag){ azért kommenteltük ki, mert nem módosítható
//            this.anyag = anyag;
//        }
        public void setSzin(Color szin){
            this.szin = szin;
        }
    }

    
